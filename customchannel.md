# Create Custom Channel

In ArrowAI platform, there are some built-in channels provided but in sequence you may add custom channel also.

If you want that your bot should be integrated with 'xyz' channel rather than the list of channels provided by ArrowAI then you can add that channel and convert it with it's responses to it. For doing so you have to follow these steps:

* Setup the ADK (ArrowAI Development Kit).
* Initialize the Project.
* Start creating your channel integration.
    * Convert Channel incoming request to ArrowAI request format.
    * Now, convert ArrowAI response format into your channel response format.
* Endpoint URL.

## Setup the ADK

Find the instructions to setup the ArrowAI Development Kit from [Here](adk.md). Once ADK is setup, continue to the next step.

##  Create New Channel Using ADK(arrowai development kit)

First create a project directory and move into that.

```bash
$ adk createchannelsdk
$ cd channelModule
```

Initialize the npm project

```bash
$ npm install 
```
## Before Start Creating Channel Integration
Add Aew Custom integration in https://cloudarrowai.web.app
 * go to https://cloudarrowai.web.app/#/external/module/integration-1211.
 * click new integration select your channel if your channel not exist here you can add custom integration.
 * after selecting  your channel fill all the details and then submit.
 * now your newly created integration will be showing in the list.

## Start creating Channel Integration

* Open file name  index.js. You may keep any name as you want.
* File will contain below code :

```js
"use strict";

class channelSdk {

    constructor(platformObject) {
        this.platform = platformObject;
    }
    receivedApiMessage(message, applicationId) {
        //Convert your channel message to arrowai message format
        let arrowaiMessageFormat =;
        this.platform.createSessionByChannelId(applicationId, '<intetgrationName>', arrowaiMessageFormat.sender.id, integrationId).then((sessionId => {
                this.platform.messageReceived(arrowaiMessageFormat, sessionId,integrationId).then((response) => {
                   //convert resoposne format to your channel format
                }).catch((error) => {
                    debug(error);

                })
            }))


    };
    sendMessage(message) {

    };
    sendBulkMessage(messages) {
        
    };
}

module.exports = channelSdk;
```

| Function Name | Description |
|---|---|
| receivedApiMessage | This function will receive the incoming channel request and convert that into ArrowAI request format that will hit our chatengine and according to that our chatengine will reponse. Then simply convert that response format into your channel response format and hit back to that same api(If this happen so). |
| sendMessage | This function is used to send message to the bot after converting it into that particular channel response format. (This would be done when we have differnet channel's request and response api.) |
| sendBulkMessage | Same as sendMessage but it is used for bulk messages. |


This is the architecture of creating a custom channel.

## Convert Channel incoming request to ArrowAI request format:

* This conversion must be performed inside the receivedApiMessage(message, applicationId)  function where message paramater receives the channel incoming request.
* Now, we simply convert that channel incoming request format into ArrowAI request format.
* The ArrowAI request format is defined [here](usermessageformat.md)

## You Can access these function of  Plateform object 

| Platform Object |
```js
    createUser: (applicationId, user, uniqueUserId) => {
        
    },
    getApplicationConfig: (sessionId) => {
        
    },
    saveIntegrationConfig: (applicationId, integration, config) => {

    },
    getIntegrationConfig: (sessionId, integration, integrationId) => {
        
    },
    getIntegrationConfigByIntegrationId: (applicationId, integration, integrationId) => {
        

    },
    saveUserConfig: (applicationId, userGroupId, config, configKey) => {
        
    },
    getUserConfig: (sessionId) => {
        
    },
  
    createSessionByUniqueUserId: (applicationId, integration, uniqueUserId, integrationIdentifier) => {
        
    },
    createSessionByChannelId: (applicationId, integration, channelId, integrationIdentifier) => {
        
    },
    closeSession: (channelId) => {
        //TODO call function by channelId
    },
    messageReceived: (message, sessionId, integrationId) => {
        
    },
    setIntegrationStorage: async (uniqueId, applicationId) => {
       

    },
    getIntegrationStorage: async (uniqueId) => {
       
    }
   

```
## function Detail
* **createUser**:
    * if you are a new user at the channel from this function you can create new user on the channel.
    * you can create new user from this api and the format of data you send to create new user is

    ```json
     {
                    "data": {
                        "appId": "<appid>",
                        "name": "<name>",
                        "source": [
                            "<integrationtype>"
                        ]
                    },
                    "deviceInfo":{},
                    "applicationId": "<appid>",
                    "channel": "<integrationtype>",
                    "userUniqueId": "<senderid>"
                }
    ```
 * platform.createUser(applicationId,json<json>, senderid);here json will be in that format as i have mentioned above.

*  **getApplicationConfig**:
   * you can get application config detail by sessionID.
    ```
         sessionID:createSessionByChannelId({
                    "channelId": arrowaiMessage.sender.id,
                    "integration": integration,
                    "applicationId": applicationId,
                    "integrationId": integrationId
                });
    ```
    * platform.getApplicationConfig(sessionID)
* **saveIntegrationConfig**:
    * you can save integration config detail data from this api .to call this api you need these values.
        ```
        applicationId:<appid>, 
        integration:<integration>,
        config:(send all the values you have mentioned in  install/main.json)
                {
                "<main.json key 1>":"value",
                "<main.json key 2>":"value",
                .......
                -------
                    }
        ```
    * platform.getApplicationConfig(applicationId,integration,config)

* **getIntegrationConfig**:
    * get all the data which you have set during your channel integration.you need these values to call         getIntegrationConfig function.
                sessionId: 
                you can get session id from this api:
                ```
                    createSessionByChannelId({
                                    "channelId": arrowaiMessage.sender.id,
                                    "integration": integration,
                                    "applicationId": applicationId,
                                    "integrationId": integrationId
                                });
               
                integration: <your integration name>
                integrationId:<created channel integrationId>
                ```
    * platform.getIntegrationConfig(sessionId,integration,integrationId)

* **getIntegrationConfigByIntegrationId**:
  *  you can get integration config detail which you have set during your channel integration.you need these values to call getIntegrationConfigByIntegrationId api.
    ```
                applicationId:<appid >
                integration:<your integration name>
                integrationId:<your integration id>
    ```
  * platform.getIntegrationConfigByIntegrationId(applicationId,integration,integrationId)
  
* **saveUserConfig**:
   * you can save userconfig values from this api.
```
                        applicationId:<applicationid>
                        userGroupId:<userGroupId>
                        config:{
                            "<main.json key 1>":"value",
                            "<main.json key 2>":"value",
                            .......
                                }
                        configKey:<userUniqueId>
```
  * platform.saveUserConfig(applicationId,userGroupId,config,configKey)

* **getUserConfig**:
    * you can get user config details from this api to call this api you need:
         SessionID:you can get sessionid by these two methods:-
        ```
            1> createSessionByUniqueUserId: (applicationId, integration, uniqueUserId, integrationIdentifier)=>{}
                applicationId:<appid>
                integration: <integration>
                uniqueUserId:<uniqueUserId>
                integrationIdentifier:<integraton id>
            2> createSessionByChannelId: (applicationId, integration, channelId, integrationIdentifier) =>{}
                applicationId:<appid> , 
                integration:<integration> , 
                channelId:<senderid> , 
                integrationIdentifier:<integrationid>
        ```
    * platform.getUserConfig(sessionId)

* **closeSession**:
    * close session by channel id.
        ```
            channelId:<senderid>
        ```
    *  platform.closeSession(channelId)

* **messageReceived**:
   *  from this api convert the arrowai response format into channel response format.
    ```
                    message:<your channel response format>, 
                    sessionId:createSessionByChannelId({
                                    "channelId": <senderid>,
                                    "integration": <integration>,
                                    "applicationId": <applicationId>,
                                    "integrationId": <integrationId>
                                });, 
                    integrationId:<integrationID>
    ```
    * platform.messageReceived(message,sessionId,integrationId) in this api message would be in arrowai         format 

    ```json
     {
            "applicationId": "appId",
            "messageData": {
                "message": { "text": "<message>", "endExistingFlow": "true" },
                "sender": { "id": "<senderid>" }
            },
            "integration": "<integrationtype>",
            "payload": {
                "integration": "<integrationtype>",
                "integrationConfig": "config"
            },
            "sender": { "id": "<senderid>" },
            "sentFromUser": true,
            "sentFromServer": false,
            "timestamp": "timestamp"
        };
    ```




* **setIntegrationStorage**:
    * from this api you can set integration storage to call this api you need:
    ```
        uniqueId:<uniqueID>, 
        applicationId:<applicationId>
    ```
    * platform.setIntegrationStorage(uniqueId, applicationId)

* **getIntegrationStorage**:
  * from this api you can get integration storage to call this api you need:
```
            uniqueId:<uniqueID>
```
 * platform.setIntegrationStorage(uniqueId)

!> you can get Integration,integrationId,and application id from your endpoint url other than this all the values mentioned here will be get from message.body or the first argument of receivedApiMessage function .
## Now, convert ArrowAI response format into your channel response format

* Before converting into your channel response format we have to call a messageReceived(arrowaiMessage) function where arrowaiMessage parameter is the ArrowAI request format which we have converted in above step like this:

```js
this.platform.messageReceived(arrowaiMessage).then((arrowaiResponse) => {
    //here you have to define your response convertor from ArrowAI to Alexa by passing `arrowaiResponse` variable as an argument.

}).catch((error) => {
    console.log(error);
})
    resolve({ success: true });
```

!> If you want to know about the ArrowAI response format which is to be converted into your channel response format then [click here](outputmessageformat.md)

* Now you may write your convertor and start execution.

## Endpoint Url:

Endpoint Url is given below:
if you have created your integration:
```bash
$ <your_server_url>/<applicationId>/<integration>/incoming/<integrationId>
```
otherwise,endpoint url can be:
```bash
$ <your_server_url>/<applicationId>/<integration>/incoming/1

```

| Variable | Type | Description |
|---|---|---|
| < your_server_url > | URL  | For testing you may use your ngrok server url i.e., https://67bf1444f4ec.ngrok.io |
| < applicationId > | Character | You have to provide your application Id |
| < integration > | String | It will be your channel type like for web integration we use 'web'. |


* Then add arrowai.json file and paste the code below-:

      {
       "unique_name": unique_name,
       "type": "channel",
       "category": "application",
       "function_deploy": {
        "enabled": "enabled",
        "function_version": "0.0.0"
       },
       "registry_deploy": {
        "registry_version": "0.0.0",
        "forced_deployment": "disabled"
       }
      }

   give unique name what you want and change the function_version and registry_version at time, when you do new changes in code.   
