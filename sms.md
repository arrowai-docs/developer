## SMS integration
 
 * [clickhere](https://cloudarrowai.web.app) and login
 * now select integration 
 * now you see channel list click on new integration
 * select SMS and fill all the details 
 * there is fields 
 ```
 apikey
 senderid
 name for integration
 ```

  
# get Apikey

  * [clickhere]("https://www.smsgatewayhub.com/Account/Login?success=true") and login .
  * you see dashboard of SMSGATEWAYHUB 
  * now click on developer tab and Select API Key
  * copy apikey
    <p align="center">
     <img src="./images/frontendelement/smsapi.PNG">
    </p>

# get Senderid

 * click on textSMS menu from dashboard smsgatewayhub
 * copy SENDERID
 * paste this in SMS integration

# name for integration

* you can give name for integration you may give any name as you want.

# Send SMS To User

* [clickhere]("https://cloudarrowai.web.app/#/") and login

* now select users from sidebar menu

* click on that user you want to send message now you see users detail there is button "send message"

* click on that send message button now pop-up will open

* in pop-up select ypur integration name and then select your component name as per your requirement

* if you want to send text then select "text"  component 

* now give the your text or your content in text fienld

* click on send button 


#  smsGateway api

* you can get all the api of smsGateway from here [clickehere]("https://www.smsgatewayhub.com/free-sms-gateway-developer-api")

 * parameter used in api of smsGateway
  
  <table>
    <tr>
      <td>API KEY</td>
      <td>String</td>
      <td>instead of username and password you can use APIKEY key for authentication of account </td>
    </tr>
    <tr>
      <td>Sender id</td>
      <td>string</td>
      <td>Approved sender id (6 characters only)</td>
    </tr>
    <tr>
      <td>channel</td>
      <td>string</td>
      <td>Message channel promotional=1 or transactional=2 amd OTP=OTP</td>
    </tr>
    <tr>
      <td>dcs</td>
      <td>string</td>
      <td>Data coding value(default is o for normal messages ,set 8 for unicode message)</td>
    </tr>
    <tr>
      <td>flashsms</td>
      <td>string</td>
      <td>flash message immediate display(default is 0 for normal message,set 1 for immediate dispaly  ) </td> 
    </tr>
    <tr>
      <td>number</td>
      <td>string</td>
      <td>recipient mobile number,pass with ", " if need need to send on more than one </td>
    </tr>
    <tr>
      <td>text</td>
      <td>text</td>
      <td>yor sms content</td>
    </tr>
    <tr>
      <td>route</td>
      <td>string</td>
      <td>pass the rout id <a href="https://www.smsgatewayhub.com/Panel/MT/MyRoutes.aspx">click here</a></td>
    </tr>
    <tr>
      <td>scheddtime</td>
      <td>string</td>
      <td>schedule date and time form scheduling message</td>
    </tr>
    <tr>
      <td>groupid</td>
      <td>string</td>
      <td>group id for numbers</td>
    </tr>
  </table>

