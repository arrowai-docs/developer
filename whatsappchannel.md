## integrate bot with whats app channel

* [clickHere](https://cloudarrowai.web.app/) and login.
* click on integration tab from left menu bar.
* now click on channel tab and list of channels will be showing here.
* now click on "new integration" button
* select whatsapp channel then pop up will open 
* <p align="center">
     <img src="./images/whatsapp.png">
    </p>
* now enter all the details of pop up window

# how to get app access token value

  * [clickHere](https://kaleyra.io/) and login.
     * after login click developers from menu.
     * click on generate api key
     * pop up window will be open and now give title name and click on save btton 
        <p align="center">
        <img src="./images/whatsapp.png">
        </p> 
        <p align="center">
        <img src="./images/whatsappapikey.png">
        </p> 

    *  now you can copy your api key from here

    <p align="center">
    <img src="./images/getapikey.png">
    </p> 

    * now paste genrated key into access token 

# App access user
 * in this you need to give the number of user ,on which number you will send message from whatsapp
 * this number will be save as which you will enter in keleyra whatsapp configuration

# Name for integration

 *  you may give any name for your integration .

 * when you fill all the details of whatsapp integration click on submit button.

 * now your integration will show in channels list.

# connect bot with integration you have created
 
 * click on integration name pop up will be open .

 * now enable bot

 * and select bot type as per your requirement it can be multi bot either it can be single bot

 * select bot from dropdown list.

 * now click on save button.

# how to connect whatspp channel with UI

 * [clickhere](https://kaleyra.io/whatsapp/numbers).

 * click on add number button popup window will be open there is fields

  ```
    Display Name
    mobile number
    incomming url
  ```
  * in display name you may give any name 

  * in mobile number field you have to enter mobile number on which mobile number you never created whatsapp account.this mobile number you will enter for  app access user during whatsapp integration creation

  * in incomming url you will enter whatsapp channel url that is like
   ```
     <serverurl or ngrok url>/<appid>/<integrationtype>/incoming/<copied integration-id>"
    ```
   *  serverurl:arrowai whatsapp channel url
   * appid:application id
   * integrationtypee:whatsapp
   * copied integration-id:it will be the integration id of that integration you have created for whatsapp channel integration


# how it works

* now save number which  you have entered in app access user field during whatsapp integration creation.

* send whatsapp message on that number this message would be any training phrase from which you have trained your bot and this bot must be connected with your integration.

* you will recieve whatsapp message that will be  bot response .

* now [clickhere](https://cloudarrowai.web.app) and select chat from left menu and what will you see your number is showing as whatsaapp user and the messages you have sent will show in the detail of your number.


 









    
