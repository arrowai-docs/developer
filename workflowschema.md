## workflow schema json

```json
{
   "name":"Test",
   "delay": {
        "type": "immidate|interval",
        "interval": "'*/2 * * * *'"
    },
   "nodes":[
      {
         "parameters":{
         },
         "name":"Start",
         "nodeId":"node-1243434",
         "nodeTypeId":"start",
         "type":"ragular",
         "typeVersion":1,
         "position":[
            0,
            0.5
         ]
      },
      {
         "parameters":{
            "timeZone":"Asia/Calcutta"
         },
         "credentials": {
        },
         "name":"Asana",
         "nodeId":"node-234567",
         "nodeTypeId":"18631365-18f1-43b5-96de-ec7ffe8620f8",
         "type":"node",
         "typeVersion":1,
         "position":[
            570,
            -20
         ]
      }
   ],
   "connections":{
      "node-1243434":{
         "main":[
            [
               {
                  "node":"Asana",
                  "nodeId":"node-234567",
                  "type":"main",
                  "index":0,
                  "delay": {
                  "type": "immidate|interval",
                  "interval": "'*/2 * * * *'"
               }
               }
            ]
         ]
         
      }
   },
   "active":false,
   "settings":{
      
   },
   "applicationId":"5ed9f323410a4ea77cde8fe9"
}
```