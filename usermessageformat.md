# Request User Message Format

The Platform will send data to your Webhook in the following formats, depending on what the user has said

## Text Message

```js
{
   "applicationId": <application_id>,
   "integration": "web",
   "messageData": {
      "message": {
         "text": <text_message>,   //i.e., hi, hey, hello
         "endExistingFlow": <boolean_value>
      },
      "sender": {
         "id": "E0274"         //generated automatically
      }
   },
   "sentFromUser": true,
   "timestamp": {
      ".sv": "timestamp"
   },
   "sender":{
      "id": "E0274"            //generated automatically
   },
   "payload": {
      "global_lat": 26.89219,          //senders latitude
      "global_long": 80.9953628,      //senders longitude
      "integration": "web"
   },
   "params":{
      "paramall_": {
         "userId": "E0274",       //generated automatically
         "groupId": "abc"         //generated automatically
      },
      "param_": {
         "userId": "E0274"        //generated automatically
      }
   },
   "message_id": "a0bf6390-b2df-11ea-b132-2ba87dece35c",
   "sentFromServer": false,
   "uniqueUserId": "E0274",      //generated automatically
   "url": "https://226307373669.ngrok.io/webhookBot"
}
```

| Title | Type | Description |
|---|---|---|
| < application_id > | Character  | Here you have to give your application id. |
| < text_message > | String | The text which you will send on the chat. |
| < boolean_value > | Boolean | The flow has to be exited or not. It should be boolean type. |

## Bottom Button

```js
{
                // "applicationId":"574c4ba63447c94e0c8b4567",
                // "botId":"574c4e563447c94e0c8b4568",
                "applicationId": window.ArrowChat.applicationId,
                "integration": "web",
                // botId: window.ArrowChat.botId,
                "messageData": {
                    "message": {
                        "text": text,
                        "postback": {
                            "payload": { value: value, variable: variable }
                        }
                    },
                    "sender": {
                        "id": window.ArrowChat.user
                    }
                },
                "sentFromUser": true,
                "timestamp": firebase.database.ServerValue.TIMESTAMP,
                "sender": {
                    "id": window.ArrowChat.user
                },
                "payload": window.ArrowChat.payload,
                params: {
                    paramall_: window.ArrowChat.paramOnce || {},
                    param_: window.ArrowChat.paramOnce || {}
                }
            };
```

| Title | Type | Description |
|---|---|---|
| < application_id > | Character  | Here you have to give your application id. |
| < Button1 >, < Button 2 > | String | The text displayed on the button. This text should indicate the purpose of the button. |
| < variable_name > | String | The name of button variable and it should be same for all the buttons. |
| < text_message > | String | The text which display on the chat. |