# Channel Introduction

Here, in ArrowAI platform there are some pre-defined (or, built-in) channel integration (i.e., Web, WhatsApp, WhatsApp Api, Facebook, Google Assistant, Android, SMS, Email, Phone) is provided.

If you want to use within these list of channels for your bot presentation you may simply integrate your bot by simply configuring to that channel and follow their corresponding steps.

By doing so you have to follow these steps:

* Login to your account.
* Go to Integrations, there you will able to see the list of channels.

<p align="center">
<img src="./images/channels.png">
</p>