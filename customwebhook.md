# Custom Webhook Bot

With the ArrowAI platform, you can also integrate Custom Bots written in any Language ( NodeJS, PHP, Python, Java, Go, etc), hosted on any Server.

If a Webhook Bot is enabled for a channel, then whenever the channel receives a message, then the platform will pass that message to the Webhook, and will send a reply to the user based on the Response of the Webhook Bot.

We will take you through step-by-step process to create a Webhook Bot.

## Coding a Webhook Bot

Webhook Bot is a simple Web API which can be running on any Server or even Serverless Platforms (like Google Functions, AWS Lambda or Azure Functions).

A sample NodeJs Webhook Bot, that replies "Thank you for Message" to any message the user sends, can be written as follows:

```js
var express = require('express');
var app = express();
var fs = require("fs");
app.get('/listUsers', function (req, res) {   
    fs.readFile( __dirname + "/" + "users.json", 'utf8', function (err, data) { 
         console.log( data );      
         res.end( data );   
    });
})

var server = app.listen(8081, function () {   
     var host = server.address().address   
     var port = server.address().port   
     console.log("Example app listening at http://%s:%s", host, port)
})
```

## Deploying the Webhook Bot

Lets say you are running this code on a server (find the Complete NodeJS Code here).

If you do not have any server you can use Hosting platforms like Heroku (Get Heroku Code and deployment Instructions here). Once you have run the code on your server, you will get a URL (something like https://{servername.com}/listUsers

You can also run the code on Serverless Platforms like Google Functions (view here)

## Adding a Webhook Bot

Once you have deployed the Bot on Server and you have the URL

## Webhook Bot vs Chat Engine Fulfillment

Please do not confuse Webhook Bot with Chat Engine Fulfillment. Webhook Bot is an entire Bot where you will have to take care of all the technicalities, including, Natural Language Processing, Entities Extraction, State Management or any other logic. The Platform provides raw request to you (whatever the user has said), and also sends the raw response to the user (whatever you respond).

This is different from the Chat Engine, where the Natural Language Processing, State Management and other things are already taken care by the Chat Engine, and the Fulfillment function only has contain specific logic in a particular flow. More about Chat Engine and Fulfillment.

# When are Webhook Bots useful?

Webhook Bots are especially useful in few cases like:

* Simple Response Logic: Lets say you have a set of words like start timer and stop timer that the system responses. In that case, a simple Webhook bot will be lot easier and will work faster.

* Integrating Existing Bots: Lets say you already have a bot developed on any other code, or platforms (like RASA, Microsoft Bot Framework, Dialogflow), and you want to Integrate with ArrowAI Platform (why integrate?), then Webhook Bots can easily Integrate them, without you having to change any Code.

* Custom Logic: Lets say you want a Custom Flow Management Logic, Custom NLP Engine, Custom Bot Management, then you can create your custom Webhook Bots.