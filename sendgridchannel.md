
## integration with sendgrid UI

* [clickhere]("https://cloudarrowai.web.app/") and click on integration from left menu

* now you see the list of channels in channel tab 

* click on new integration button 

* select send-grid and then pop-up will open

*  fill all the details there is fields ,after fill all the fields click submit button
   
   ```
   * apikey
   * fromemail
   * fromname
   * name for integration
   ```

# get api key 

* [clickhere](https://app.sendgrid.com/) and login

* for sign up or login you must have emailId having company daomain.

* click on settings tab and copy API Keys

  <p align="center">
  <img src="./images/sendgridapi.png">
  </p>

* now paste api key in api key field during arrow ai  sendgrid integration .

# from email

* in this field  you have to enter email id remember, you have to registered with with email id in sendgrid

# from name
 
* here,enter your name from that name you can send email

# Name for integration

* in this field enter integration name you may give any name for integration

## send message to user

* [clickhere]("https://cloudarrowai.web.app/#/") and login

* now select users from sidebar menu

* click on that user you want to send message now you see users detail there is button "send message"

* click on that send message button now pop-up will open

* in pop-up select your integration name ("sendgrid") and then select your component name as per your requirement

* if you want to send text then select "text"  component 

* now give the your text or your content in text fienld

* click on send button 






