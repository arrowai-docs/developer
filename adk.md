# ADK - ArowAi Development Kit

ArrowAI Development Kit is a SDK Kit to facilitate the Development of different modules for the ArrowAI Platform

## Installation of ADK

* Copy the code from [Here](https://storage.googleapis.com/arrowaisdk-packages/adk/adk.tar)
* Run the Command:  npm i -g adk

## ADK Usage

ADK helps users in the development of ArrowAI Modules, and also provides them an option to locally develop, test and deploy ArrowAi components. Some of the Commands are detailed below.

### 1. adk createchannelsdk

* This creates a boilerplate for Channel Development. See Details about [what is channel](channelintro.md)

### 2.adk createfrontendsdk
 
 * This creates a boilerplate for frontend Development. See Details about [what is element](frontendelement.md)




