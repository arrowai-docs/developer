## Types of Node 
### simple node
### trigger node
### segment Trigger
### event Trigger

# Simple Node Def example

  {
    "fulfillmenttype": "external",
    "icon": "fa: fa-mail",
    "status": "live",
    "version": "1",
    "nodename": "Asana",
    "uniquename": "asana-node",
    "fulfilmenturl": "https://asia-south1-arrowai-kubernetes.cloudfunctions.net/workflow-node-asana",
    "nodeType": "node",
    "inputs": [
      "main"
    ],
    "category": "",
    "group": "application",
    "description": " Description",
    "noOfFailedAttempts": "0",
    "color": "#449922",
    "installProperties": "[]",
    "outputs": [
      {
        "name": "pass",
        "type": "immidiate"
      },
      {
        "name": "failed",
        "type": "immidiate"
      }
    ],
    "nodeTypeId": "8fb144bb-9291-4a66-8cd5-c7c7e0fd0c54",
    "actions": [],
    "properties": []
  }
### Node With Wait output
 {
    "nodename": "E-mail",
    "uniquename": "email-node",
    "description": " Description",
    "nodeType": "node",
    "inputs": [
      "main"
    ],
    "category": "",
    "icon": "fa: fa-mail",
    "status": "live",
    "noOfFailedAttempts": "0",
    "installProperties": "[]",
    "outputs": [
      {
        "name": "sent",
        "type": "immidiate"
      },
      {
        "name": "failed",
        "type": "immidiate"
      },
      {
        "name": "viewed",
        "type": "wait"
      },
      {
        "name": "clicked",
        "type": "wait"
      }
    ],
    "version": "1",
    "group": "application",
    "fulfillmenttype": "external",
    "fulfilmenturl": "https://asia-south1-arrowai-kubernetes.cloudfunctions.net/workflow-node-asana",
    "color": "#449922",
    "nodeTypeId": "1809fbad-f762-4d3c-ba68-3bc97f201a36",
    "actions": [],
    "properties": []
  },

### Segment Trigger Node
  {
    "group": "application",
    "status": "live",
    "description": " Description",
    "fulfillmenttype": "external",
    "icon": "fa: fa-mail",
    "uniquename": "asana-node-trigger",
    "inputs": [],
    "noOfFailedAttempts": "0",
    "version": "1",
    "fulfilmenturl": "",
    "installProperties": "[
  {
    "displayName": "Category",
    "name": "category",
    "type": "options",
    "options": [
      {
        "name": "entry",
        "value": "entry"
      },
      {
        "name": "Exit",
        "value": "exit"
      }
    ],
    "default": "entry",
    "description": "Action Type"
  },
  {
    "displayName": "segmentId",
    "name": "segmentId",
    "type": "string",
    "description": "segmentId"
  }
]",
    "color": "#449922",
    "nodename": "Segment Trigger",
    "outputs": [
      {
        "name": "main",
        "type": "immidiate"
      }
    ],
    "nodeType": "segmentTrigger",
    "nodeTypeId": "b4a3d815-1c3a-4c2d-81bd-2a5857dcbb5d",
    "actions": [],
    "properties": [
      {
        "displayName": "Category",
        "name": "category",
        "type": "options",
        "options": [
          {
            "name": "entry",
            "value": "entry"
          },
          {
            "name": "Exit",
            "value": "exit"
          }
        ],
        "default": "entry",
        "description": "Action Type"
      },
      {
        "displayName": "segmentId",
        "name": "segmentId",
        "type": "string",
        "description": "segmentId"
      }
    ]
  },
  ### Event Trigger Node
  {
    "fulfilmenturl": "",
    "installProperties": [
  {
    "displayName": "Category",
    "name": "category",
    "type": "options",
    "options": [
      {
        "name": "entry",
        "value": "entry"
      },
      {
        "name": "Exit",
        "value": "exit"
      }
    ],
    "default": "entry",
    "description": "Action Type"
  },
  {
    "displayName": "Event Type",
    "name": "eventType",
    "type": "options",
    "options": [
      {
        "name": "email Clicked",
        "value": "emailClicked"
      },
      {
        "name": "Ticket Raised",
        "value": "ticktRaised"
      }
    ],
    "default": "emailClicked",
    "description": "Event Type"
  },
  {
    "displayName": "emailTemplateId",
    "name": "emailTemplateId",
    "type": "string",
    "description": "emailTemplateId"
  }
],
    "version": "1",
    "inputs": [
      "main"
    ],
    "nodeType": "eventTrigger",
    "group": "application",
    "uniquename": "event-node-trigger",
    "color": "#449922",
    "noOfFailedAttempts": "0",
    "nodename": "Event Trigger",
    "outputs": [
      {
        "name": "main",
        "type": "immidiate"
      },
      {
        "name": "timeOut",
        "type": "immidiate"
      }
    ],
    "icon": "fa: fa-mail",
    "status": "live",
    "description": " Description",
    "fulfillmenttype": "external",
    "nodeTypeId": "d09f6f4c-4e81-4649-a515-354c22753bd1",
    "actions": [],
    "properties": [
      {
        "displayName": "Category",
        "name": "category",
        "type": "options",
        "options": [
          {
            "name": "entry",
            "value": "entry"
          },
          {
            "name": "Exit",
            "value": "exit"
          }
        ],
        "default": "entry",
        "description": "Action Type"
      },
      {
        "displayName": "Event Type",
        "name": "eventType",
        "type": "options",
        "options": [
          {
            "name": "email Clicked",
            "value": "emailClicked"
          },
          {
            "name": "Ticket Raised",
            "value": "ticktRaised"
          }
        ],
        "default": "emailClicked",
        "description": "Event Type"
      },
      {
        "displayName": "emailTemplateId",
        "name": "emailTemplateId",
        "type": "string",
        "description": "emailTemplateId"
      }
    ]
  },

### Trigger Node
  {
    "fulfilmenturl": "https://asia-south1-arrowai-kubernetes.cloudfunctions.net/workflow-node-trigger-asana",
    "installProperties": "[]",
    "noOfFailedAttempts": "0",
    "nodeType": "trigger",
    "icon": "fa: fa-mail",
    "uniquename": "segment-node-trigger-exit",
    "outputs": [
      {
        "name": "main",
        "type": "immidiate"
      }
    ],
    "version": "1",
    "description": " Description",
    "nodename": "Asana Trigger",
    "group": "application",
    "color": "#449922",
    "category": "",
    "fulfillmenttype": "external",
    "status": "live",
    "inputs": [],
    "nodeTypeId": "e0d0987f-1089-41b7-a94f-48f492d9cb10",
    "actions": [],
    "properties": []
  }
]

## How tocreate node

* downlod this zip file.
 <a href="./zip repositories/workflow-node.zip" download="workflow-node.zip">Workflow node</a>

* after extracting  zip repositories open "install.json".
* copy json from above according to your node type and paste in install.json.
* in install.json there is installProperties object 
  * here you have to write those things which you need from user.
  * your install.json will be like

  ```
  json
  {
    "fulfillmenttype":"external",
    "icon":"fa: fa-mail",
    "status":"live",
    "version":"1",
    "nodename":"<node name>",
    "uniquename":"<node unique name>",
    "fulfilmenturl":"https://asia-south1-arrowai-kubernetes.cloudfunctions.net/workflow-node-asana",
    "nodeType":"node",
    "inputs":[
       "main"
    ],
    "category":"",
    "group":"application",
    "description":" Description",
    "noOfFailedAttempts":"0",
    "color":"#449922",
    "credentials":[
    <credentials>
   ],
   
    "installProperties":[
      <set properties you need from user>
   ],
    "outputs":[
       {
          "name":"pass",
          "type":"immidiate"
       },
       {
          "name":"failed",
          "type":"immidiate"
       }
    ],
    "actions":[
       
    ],
    "properties":[
       
    ]
 }

 
  * you can do all your code into executeFunction.js.
  * except these two files you don't need to touch any other file

  # functions to get all the values send by user
    * getCredentials()
    * getInputData()

   * we can get credentials from below code
    ```js
    let credentials=await this.platformObj.getCredentials()

    ```
  * we can get all data send by user from below code

  ```js
  this.platformObj.getInputData()

  ```
  * from above line of code in platformObj there is function getInputData() from this fnction you can get 
   all the value send by user.

   ## if you are testing from postman
    
     <a href="./workflow-gateway-api.postman_collection.json" download="workflow-gateway-api.postman_collection">Workflow_collection download</a>


# how to run this code on local

  * run command given below

   ```
   npx @google-cloud/functions-framework --target=executeNode

   ```
   
# push on server 

 * clone "workflow " repository  .
 * paste your workflow-node repository here and then push your code 

# how to push code 

 * open arrowai.json of your workflow-node repository
 ```json
 {
    "type": "cloud_function",
    "unique_name": "unique name",// this name should be unique
    "category": "application",
    "function_deploy": {
        "enabled": "enabled",
        "function_version": "0.0.0"//increase by one always when you push your code
    },
    "registry_deploy": {
        "registry_version": "0.0.0",//increase by one always when you push your code
        "forced_deployment": "disabled"
    },
    "cloud_function": {
        "entrypoint": "executeNode"
    }
}
 ```
 * increase  registry and function version  by one and then push code.
 * run command

  * git add .
  * git commit -m 'changes'
  * git push

 ## create trigger

   * To create trigger node download  file
    
 <a href="./zip repositories/workflow-node-trigger.zip" download="workflow-node-trigger.zip">Workflow node trigger</a>

* extract file and open install.json in this repositories.
* in install.json paste the above Trigger Node json as per your trigger node type .
* you have to set credentials object to get the credential of user.
* like  node in trigger also you have to set properties in installProperties object these property will be that you need to get from user.your install.json will be look like this.
      ```json
          {
            "nodename":"E-mail",
            "uniquename":"email-node",
            "description":" Description",
            "nodeType":"node",
            "inputs":[
                "main"
            ],
            "category":"",
            "icon":"fa: fa-mail",
            "status":"live",
            "noOfFailedAttempts":"0",
            "credentials":[
              "code to get credential"
            ],
            
            "installProperties":[
          "set properties you need from user"
            
          
            ],
            "outputs":[
                {
                  "name":"sent",
                  "type":"immidiate"
                },
                {
                  "name":"failed",
                  "type":"immidiate"
                },
                {
                  "name":"viewed",
                  "type":"wait"
                },
                {
                  "name":"clicked",
                  "type":"wait"
                }
            ],
            "version":"1",
            "group":"application",
            "fulfillmenttype":"external",
            "fulfilmenturl":"https://asia-south1-arrowai-kubernetes.cloudfunctions.net/workflow-node-trigger-asana",
            "color":"#449922",
            "actions":[
                
            ],
            "properties":[
                
            ]
          }

      ```

* like node in trigger node also you need to changes in only two files i.e. install.json and executeTrigger.js

## trigger functions

 * there is some funtions you can use in  trigger as per your requirment.these functions are defined in executeTrigger.js
  * triggerTimeDefinition
  * checkTriggerInvocation
  * checkTrigger
  * webhookRequest

  # triggerTimeDefinition

    * from this trigger function we can define after how much trigger will work.
    * function definition
     ```js
     async triggerTimeDefinition(){
        var triggerdef = {
            recurring: true,
            interval: "*/5 * * * *"
        };
      
        return triggerdef;
    }
     ```

  # checkTriggerInvocation

    * In this trigger function we can check condition fullfilled or not 
    * function definition 
    ```js
      async checkTriggerInvocation(){
        var triggerInvocationSuccess = {
            invoke: true,    //Could be set to False if your Condition is not satified
            initialParams: {
                "Key": "Value"
            }
        }
        return triggerInvocationSuccess
    }
    ```
    * set invoke false if  your Condition is not satisfied

  # checkTrigger

    * in this trigger function it will check from UI trigger is working or not.
    * function definition
    
    ```js
    async checkTrigger(){
        var manualTrigger = {
            initialParams: {
                "Key": "Value"
            }
        }
        return manualTrigger;
    }
    ```

  # webhookRequest

  ## How to run your code on local

      * run command given below

   ```
   npx @google-cloud/functions-framework --target=executeTriggerNode

    ```
  <!-- <a href="./workflow-gateway-api.postman_collection" download="workflow-gateway-api.postman_collection">Workflow_collection</a> -->

  ## here is post man collection if you want to do testing from postman
  
      <a href="./workflow-gateway-api.postman_collection.json" download="workflow-gateway-api.postman_collection">Workflow_collection download</a>

  
  * To test trigger from postman you can use collection above given and in your postmant url 
   you will give the name of  function you are using as per your requirement.

   * you url will be like
         "http://localhost:8080/<yourfunctionname>"
  
  ## push your code on server

 * clone "workflow " repository  .
 * paste your workflow trigger repository here and then push your code 

# how to push code 

 * open arrowai.json  of your trigger repository
 ```json
 {
    "type": "cloud_function",
    "unique_name": "unique name",// this name should be unique
    "category": "application",
    "function_deploy": {
        "enabled": "enabled",
        "function_version": "0.0.0"//increase by one always when you push your code
    },
    "registry_deploy": {
        "registry_version": "0.0.0",//increase by one always when you push your code
        "forced_deployment": "disabled"
    },
    "cloud_function": {
        "entrypoint": "executeTriggerNode"
    }
}
 ```
 * increase  registry and function version  by one and then push code.
 * run command

  * git add .
  * git commit -m 'changes'
  * git push

## how to import postman collections

 <p align="center">
  <img src="./images/postmancollection.png">
 </p> 

* open postman window click import button then popup will open upload postman collection which you have download after uploaded sucessfully now you can see uploaded file in collections tab .

* after postman collection successfully upload click on your from postman ->collections tab .
* now you can see there is 7 requests and to test your trigger or node you can use any  api from these 7 requests as per your requirement.

