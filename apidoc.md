
# To get NLP

 * "https://chatengine.arrowai.in/chats/detectIntent" from this api you will get the detail of nlp.

 * format for this api

 ```json
{
                "applicationId":"<appid>",
                "integration":"<integration>",
                   "message": {
                         "text": "<text>"
                     },
                "sentFromUser":true,
                "timestamp":{
                   ".sv":"2020-07-27T07:35:51Z"
                },
                "sender":{
                   "id":"admin"
                },
               
                "message_id":"980ba758-97cb-4d64-9aa6-ddbd4a378b69",
                "sentFromServer":false,
                "botId":"<botId>"
             }
 ```

<html>
<table>
<tr>
<td>appid</td>
<td>your application id</td>
</tr>
<tr>
<td>integration</td>
<td>integration type of your integration</td>
</tr>
<tr>
<td>text</td>
<td>message you want to ask from bot</td>
</tr>
<tr>
<td>botid</td>
<td>bot's Id </td>
</tr>
</table>
</html>

# get response from bot 
 
  ## if you have nlp detail 

  * if you have nlp detail and you want to get response from bot  you need to call "https://chatengine.arrowai.in/chats/callAfterNLP" post api.

  * format for this api is
```json
  {
    "applicationId": "<appid>",
    "integration": "<integration>",
    "messageData": {
        "message": {
            "text": "<text>"
        }
    },
    "sentFromUser": true,
    "timestamp": {
        ".sv": "2020-07-27T07:35:51Z"
    },
    "sender": {
        "id": "admin"
    },
    "message_id": "980ba758-97cb-4d64-9aa6-ddbd4a378b69",
    "sentFromServer": false,
    "botId": "<botid>",
    "intent": {
        "trigger": {
            "flow": "<message.selectedNLP.matchedIntent>",
            "_id": "<message.selectedNLP.matchIntentId>",
        }
    }
}
```
<html>
<table>
<tr>
<td>appid</td>
<td>your application id</td>
</tr>
<tr>
<td>integration</td>
<td>integration type of your integration</td>
</tr>
<tr>
<td>text</td>
<td>message you want to ask from bot</td>
</tr>
<tr>
<td>botid</td>
<td>bot's Id </td>
</tr>
</table>
</html>

  * "message.selectedNLP.matchedIntent" and "message.selectedNLP.matchedIntent" value you can will get from [click here]("https://arrowai-docs.gitlab.io/developer/#/apidoc") api 


  ## if you don't have nlp detail 

  * if you want to get response from bot and you don't have nlp detail you need to call 
    api  "https://chatengine.arrowai.in/chats/callBeforeNLP" post api

  *  format
 ```json
   
   {
    "applicationId": "<appid>",
    "integration": "<integration>",
    "messageData": {
        "message": {
            "text": "<text>"
        }
    },
    "sentFromUser": true,
    "timestamp": {
        ".sv": "2020-07-27T07:35:51Z"
    },
    "sender": {
        "id": "admin"
    },
    "message_id": "980ba758-97cb-4d64-9aa6-ddbd4a378b69",
    "sentFromServer": false,
    "botId": "<botid>",
    "intent": {
        "trigger": {
            "flow": "<message.selectedNLP.matchedIntent>",
            "_id": "<message.selectedNLP.matchIntentId>",
        }
    }
}
```


<html>
<table>
<tr>
<td>appid</td>
<td>your application id</td>
</tr>
<tr>
<td>integration</td>
<td>integration type of your integration</td>
</tr>
<tr>
<td>text</td>
<td>message you want to ask from bot</td>
</tr>
<tr>
<td>botid</td>
<td>bot's Id </td>
</tr>
</table>
</html>
    


 






