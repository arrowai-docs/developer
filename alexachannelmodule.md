# create server for your alexa bot to get responses 
 * create node.js project
  * create index.js(you can give any name).

 ```js
const express = require('express');
const app = express();
var router = express.Router();
const bodyparser = require('body-parser');
const verifySignature=require('./signature')
app.use(bodyparser.json());
app.post('/', async(req, res) =>{
    console.log(req);
    let info = await verifySignature.hello(req.body, req.headers);
    res.send({
        version: '1.0',
        response: {
        shouldEndSession: false,
        outputSpeech: {
            type: 'SSML',
            text: 'Hello World!',
            ssml: '<speak>Hello World!</speak>'
            }
        }
    })
});
app.listen(3000, () => console.log('Example app listening on port 3000!'));
module.exports = router;
```
* run your server from "npm start" or "node index.js".
* create ngrok url "ngrok http <portnumber>"

# create alexa bot 
* go to this url https://developer.amazon.com/alexa/console/ask then signin 
* after signin create skill ex-"funnybot".
    * then choose model :"custom" .
    * Choose method to host your skill's backend resources: "Provision your own".and click "create skill".
    * Choose template to add to your skill:"start from scratch".and click "continue with template".
* set invocation name ex-"funny bot".
* got to custom->interaction model->json editor paste this json:

 ```json
 {
    "interactionModel": {
        "languageModel": {
            "invocationName": "(your invocation name ex:-) funny bot",
            "intents": [
                {
                    "name": "AMAZON.FallbackIntent",
                    "samples": []
                },
                {
                    "name": "AMAZON.CancelIntent",
                    "samples": []
                },
                {
                    "name": "AMAZON.HelpIntent",
                    "samples": []
                },
                {
                    "name": "AMAZON.StopIntent",
                    "samples": []
                },
                {
                    "name": "AMAZON.NavigateHomeIntent",
                    "samples": []
                },
                {
                    "name": "commandbot",
                    "slots": [
                        {
                            "name": "command",
                            "type": "MyCustomSlotType"
                        }
                    ],
                    "samples": [
                        "{command}"
                    ]
                }
            ],
            "types": [
                {
                    "name": "MyCustomSlotType",
                    "values": [
                        {
                            "name": {
                                "value": "do something"
                            }
                        },
                        {
                            "name": {
                                "value": "something"
                            }
                        }
                    ]
                }
            ]
        }
    }
}
```

* go to the Interaction Model->Endpoint select "HTTPS" 
 * in default region  set your <arrowaialexachannelurl>  or your bot  ngrok url and in dropdown select second         option i.e.
   "your development endpoint is subdomain of a domain that has wildcard certificate from a certificate authority".
   set these two things in all of the optional fiels :-
    North America,Europe and India,Far East.

    arrowaialexachannelurl :"https://alexa-channel-6596-srnzdkes3a-as.a.run.app/<appid>/                               <integrationtype>/incoming/<integrationid>"	

* after setting all the things mentioned in previous line click on "Save Endpoints" button .
* when you have completed all the things mentioned above click on invocation now you are on invocation page click on "Save Model" and then "Build Model" .


# test your bot

 * go to the "Test" tab and enable development (if Skill testing is enabled in:"off")
 * say or type:"ask <yourinvocationname> whatever you want to say".

 # test alexa-channel-module:

 * go to this url https://cloudarrowai.web.app
 * create custom integration from integration menu (it will be same as alexa endpoint url)
        set webhook url :<serverurl/ngrok-url>/<application-id>/alexa/incoming/1
        write your integration name:"alexa-integration"(you may give any name).
    * go to this url https://cloudarrowai.web.app/#/integrations/channels and click on "alexa-integration"
    check on enable bot and then select single bot or multi bot (whatever you want).
    * after selecteing bot category in "module name" dropdown select any bot which you want to connect with alexa.
    * copy integration id of "alexa-integration" pass this integration id into your alexa bot endpoint url.
    url must be look like:
    <server-url/ng-rok-url>/<applicationid>/alexa/incoming/<copiedintegration-id>
    * start alexa channel module with "adk startchannelsdk".
    * create your ngrok url on 3000 port.

    * your alexa endpoint url will be 
      <ngrokurl>/<Applicationid>/<integrationtype>/incoming/<copiedintegration-id>
    



