- [Overview](/)

- Setting Up Workspace
  - [ADK - ArrowAI Development Kit](adk.md)
  - [Gitlab Setup](gitlabsetup.md)

- ArrowAI Platform

  - [User Message Format](usermessageformat.md)
  - [Output Message Format](outputmessageformat.md)

- Custom Bot Development

  - [Custom Webhook Bot](customwebhook.md)
  - [Custom Bot Connector](botconnector.md)

- Bot Development

  - [Dialog Flow Bot](dialogflow.md)

- Custom Channel Development

  - [Channel Integration](channelintro.md)
  - [Custom Channel](customchannel.md)

- Frontend Integration
  - [Element Integration](frontendelement.md)


- Custom API Development
  - [API Overview](apimoduleoverview.md)

- Api Doc
  - [APIDoc](apidoc.md)

- Workflow

- Workflow 
    - [Nodes](workflownodes.md)
    - [Overview](workflowschema.md)
    - [schema for queue store](workflowstepinqueue.md)
    - [schema individual nodes](workflowstepsforindividual.md)

- Channel
   - [web](web.md)
   - [alexa](alexachannelmodule.md)
   - [Googleaction](googleactionchannel.md)
   - [whatsapp](whatsappchannel.md)
   - [facebook](facebookchannel.md)
   - [sendgrid](sendgridchannel.md)
   - [sms](sms.md)
   - [slack](slack.md)
