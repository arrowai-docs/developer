## Setup the ADK

Find the instructions to setup the ArrowAI Development Kit from [Here](adk.md). Once ADK is setup, continue to the next step.

##  Create New Element Using ADK(arrowai development kit)

* First create a project directory and move into that.

```bash
$ adk createfrontendelementsdk
$ cd elementModule
```

* Initialize the npm project

```bash
$ npm install 
```

 At the first time it will take some time.

* Then create one component in src/app folder by using following command-:
    
      ng g c component_name

 note- component_name will be depends on you what you want to give.

* After this four files are generated in component folder are as follows-:
  
      component_name.component.html
      component_name.component.css
      component_name.component.spec.ts
      component_name.component.ts

 you can write the code in this file. You can also generate more components as per requirements it will be totally depends on you.

* then open app.module.ts,
  
    change parameter in customElements.define as 'element-module'.
    <p align="center">
     <img src="./images/frontendelement/app-module.PNG">
     </p>

* open app-routing.module.ts,

  import components that you generated and set the routes as per. Define outlet also for e.g employeeModule, quizModule, leaveModule etc. Below is the snapshot of code -:
    <p align="center">
     <img src="./images/frontendelement/app-routing.PNG">
    </p>


* In app.component.html, there is predefiend router-outlet tag, change the name field as per outlet   define in app-routing.module.ts.

 <p align="center">
     <img src="./images/frontendelement/appcomponent.PNG">
 </p>

* In app-component.ts,
     in ngOnInit, there is functions like setHeadertabs(), setTitle(), setBreadcrumbs(), setActiveTab()       
  change the fields as per you want.

* Then open arrowai.json,

    change the unique_name field that would relatable to your element. Set the registry_version to 0.0.1
    Now add the following code below the regisrry_deploy -:
          "ui_element": {
                          "component_name": component_name,
                          "menu_name": menu_name,
                          "menu_icon": icon,
                          "description": description
                        }
    <p align="center">
     <img src="./images/frontendelement/arrowai.PNG">
     </p>
    
  * Set name in package.json and package.lock.json same as unique_name in arrowai.json.

   <p align="center">
     <img src="./images/frontendelement/package.PNG">
   </p>
  <p align="center">
     <img src="./images/frontendelement/package1.PNG">
  </p>

        Then run command -:
                             adk startfrontendsdk

 Final result is as follows-:
 <p align="center">
  <img src="./images/frontendelement/final.PNG">
 </p>                         

  * At the time of uploading code on server change parameter in customElements.define as component_name define in arrowai.json and lowercase the e of elementEvent in app.component.ts and also update the the registry varsion in arrowai.json.

 <p align="center">
  <img src="./images/frontendelement/app-module1.PNG">
 </p> 

 # How to send event from arrowai element

  * paste this code into app.component.ts

   @Output('elementevent') elementevent = new EventEmitter();

  * To send any event use this function :
  ```
      sendEvent(event) {
      this.elementevent.emit(event);
      }
      here,event= event we need to send.
          elementevent=  from @output properties. 
  ```

## Go to app.components.ts and paste this code
```
import { Component, Input, Output,ChangeDetectorRef, EventEmitter, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import '@vaadin/vaadin-button';
import '@vaadin/vaadin-grid';
import '@vaadin/vaadin-text-field';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit,OnChanges {
  tabs = [{
    name: <tabname>,
    url: <tabname>,
    baseRoute: <tabname>
  },
]

  constructor(private router:Router){}
  @Input('applicationid') applicationid: string; // decorate the property with @Input();
  @Input('selected_tab_url') selected_tab_url: string; // decorate the property with @Input()
  @Output('elementevent') elementevent = new EventEmitter();
  ngOnInit() {
    this.setHeaderTabs();
    this.setTitle();
    this.setBreadcrumbs();
    this.setActiveTab();
    this.router.initialNavigation();
  }

  setHeaderTabs() {
    let event = {
      method: 'setHeaderTabs',
      data: {
        tabs: this.tabs
      }
    }
    this.sendEvent(event);
  }

  setBreadcrumbs() {
    let event = {
      method: 'setBreadcrumbs',
      data: {
        breadcrumbs: [
          { title: <your title name>, page: `/<url>` },
        ]
      }
    }
    this.sendEvent(event);
  }

  setActiveTab() {
    let event = {
      method: 'setActiveTab',
      data: {
        activeTab: 1
      }
    }
    this.sendEvent(event);
  }

  setTitle() {
    let event = {
      method: 'setTitle',
      data: {
        title: <title you want to set>
      }
    }
    this.sendEvent(event);
  }

  sendEvent(event) {
    this.elementevent.emit(event);
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
    for (const propName in changes) {
      if (changes.hasOwnProperty(propName)) {
        console.log(propName)
        switch (propName) {
          case 'applicationid': {
            console.log(changes[propName].currentValue);
            localStorage.setItem('appid',changes[propName].currentValue);
            this.<your service>.publishEvent(changes[propName].currentValue);
              
              }  
          }
        }
      }
    }
  }

```
* in your service  "publishEvent"(you may give any name) function will be 
  ```
  publishEvent(appid){
      console.log("got applicationId",appid);
      this.customEvent.next(appid);
    }
  ```
## events we can use in our element

* there is many events we can use while making arrowai module element .some events we are using in             app.component.ts
* events that we are using in app.component.ts :
  ```
  1>setHeaderTabs
  2>setBreadcrumbs
  3>setActiveTab
  4>setTitle
  5>getActiveUser
  ```
 

  # setHeaderTabs

    *  To set the header we use this event "setHeaderTabs".
    *  you can send this event
      ```
      let event = {
      method: 'setHeaderTabs',
      data: {
        tabs: <tab's json>
      }
    } 
    
    

  * to send this event call sendEvent() function and pass event that is mentioned above as an arguement.
  ```
  sendEvent(event);
  ```
  
  #  setBreadcrumbs
   
   * To set breadcrumbs in your arrowai element send "setBreadcrumbs" event.
   * you can send setBreadcrumbs" event :
    ```
    let event = {
      method: 'setBreadcrumbs',
      data: {
        breadcrumbs: [
          { title: <breadcrumb title name>, page: `/<url>` },
        ]
      }
    }
  here, <breadcrumb title name> = title you want set as your breadcrumb title,
        <url> = you can give that url where you want to redirect your page after click on breadcrumb

    ```

    # setActiveTab
    
    * you can send setActiveTab event in your arrowai element 
 
   ```
     let event = {
      method: 'setActiveTab',
      data: {
        activeTab: 1
      }
    }
   ``` 
    * send setActiveTab by calling sendEvent function .
   # hideSubHeader
    
   * you can send hideSubHeader event in your arrowai element 
 
   ```
     let event = {
      method: 'hideSubHeader',
      data: {
        showHide: true
      }
    }
   ```

  * send hideSubHeader by calling sendEvent function .
   # sendMessageToUser event

     * sendMessageToUser event from this  event you can send message to user in this event by email,sms .to send message to your desired user .to send user message you need to userProperties like user id,user email,user name.

    * you can define sendMessageToUser like below
    ```` 
    let event = {
      method: 'sendMessageToUser',
      data:{
			"userSegmentType": "user",
			"when": {
				"dates": [
					{
						"date": "",
						"time": ""
					}
				],
				"start": "now",
				"end": "neverEnd"
			},
			"channel": "",
			"integrationId": "",
			"what": {
				"component": "Text",
				"list": [
				],
				"card": [
				],
				"text": "",
				"video": "",
				"image": {
				},
				"document": {
				},
				"audio": {
				}
			},
			"who": [
				<userDetail>
			]
		}
    }
    
    ```
* in data <userdetail> is the detail of recipient
* now send this event by calling sendEvent function .
* when the event sucessfuly send popup will be open

## more events that can be used in arrowai element

  * getActiveUser
  * installedModules
  * sendMessageToUser



# how these events works:
  * create service run command

  ```
   ng g service communication
  ```
  * go to your communication.service.ts and paste this code:
      ```
      import { Injectable } from '@angular/core';
      import { BehaviorSubject, Observable } from 'rxjs';

        @Injectable({
          providedIn: 'root'
        })
        export class CommunicationService {
          public windowEvent: BehaviorSubject<Observable<any>> = new BehaviorSubject<Observable<any>>(null);
          public customEvent: BehaviorSubject<Observable<any>> = new BehaviorSubject<Observable<any>>(null);
          constructor() { 

          }
          sendEvent(event){
            this.customEvent.next(event);
          }
        }

      ```
 * now go to app.component.ts 
 * import service which you have creates for here communication.service.ts.
    ```
    import { CommunicationService } from <service file path>;

    ```
    * now declare your service in constructor:
    ```
     constructor(private communication:communicationService){}

     ```
     * at the place of communication you may give any name.
     * paste this code in app.component.ts in ngOnInit() to send event mentioned above:

     ```
      this.communication.customEvent.subscribe(event=>{
                let data = {
                <event you want to send>
                }
                this.sendEvent(data);
              })
      ```
      * to get the reponse of above event you have  send from above line paste this code in ngOnInit()  
      ```
       window.addEventListener("customEvent", function(event) {
            if(!!event['detail'] && event['detail']!=null )
            self.communication.windowEvent.next(event['detail']);
          });
      ```
      * now you can get this response in your file in which file you want to get event response.

      * to get event response in your "file" paste below code:
        ```
        this.communication.sendEvent({});
        this.communication.windowEvent.subscribe(event=>{
      
          var eventData = event;

        })
        ```

  # getActiveUser event:

      * from this event we can get all the properties of activeuser and your application.
      * you can define this event like below 
        ```
        let event = {
			method: 'getActiveUser',
			data: {
			  type:'activeUser'
			}
		}

        ```
    * so,you can call this event 
    ```
    this.communication.customEvent.subscribe(event=>{
                let data = {
                  method: 'getActiveUser',
                  data: {
                    type:'activeUser'
                  }
                }
                this.sendEvent(data);
              })
    ```
    * you can get this event response as i have mentioned above.

# getInstalledModule event
* from this event you can get installed element on the basis of desired category
* the definition of this getInstalledModule event will be
 ```
 let event={
   method: 'getInstalledModule',
      data: {
        type: <which category of element you want to get>
      }
      }
 ```
 * so,you can call this event 
    ```
    this.communication.customEvent.subscribe(event=>{
                let data = {
             method: 'getInstalledModule',
              data: {
                type: <which category of element you want to get>
              }
            }
                this.sendEvent(data);
              })
    ```
* you can get this event response as i have mentioned above.in the response of this event you will get 
    installed module of particular category which you have defined in your event definition.

## How to show all installed element on your browser:
* paste this code on your componentname.component.html
```
 <ng-container  *ngFor="let module of installedModule">
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label" (click)="loadElement(module)">{{module}}</label>
                                    </div>
                                </ng-container>
   <div  *ngIf="showingElement"class="kt-grid__item kt-grid__item--fluid kt-app__content" [innerHtml]="content">
```

* paste this code in componentname.component.ts 

```
installedModule:any;
 this.communication.sendEvent({});
        this.communication.windowEvent.subscribe(event=>{
      
          this.installedModule = event['installedModuled'];

        })
```
```

public loadScript(url, name) {
  console.log('preparing to load...')
  let node = document.createElement('script');
  node.src = url;
  node.id = name,
  node.type = 'text/javascript';
  node.async = true;
  node.charset = 'utf-8';
  console.log(node);
  document.getElementsByTagName('head')[0].appendChild(node);
}
```
```
loadElement(el){
  this.showingElement=true
  this.loadScript(el.url, el.name);
  let element = `<${el.componentName} ></${el.componentName}>`
  console.log(element,el);
  this.content = this.domSanatizer.bypassSecurityTrustHtml(element);
  this.cdr.detectChanges();
  // setTimeout(function () {
  this.component = document.querySelector(el.componentName);
  this.component["applicationid"] =""
  this.component["selected_tab_url"] = '/';
  this.component.addEventListener('elementevent', function (event) {
    console.log(event);
    
  });
}

```
* here,communication is service it is same as i mentioned in above.







   


    







       


     

  
     



    
      




                        


      




