# Custom Bot Connector

* [clickHere](https://cloudarrowai.web.app/) and login.
* go to https://cloudarrowai.web.app/#/external/module/bots-1928.
* select your bot then you wil be redirect to the flows list of your selected bot.
* after click on functions tab.
  there is two option :1>webhook url 2>inbuilt functions
* select webhook url then input box will come:
  paste here your webhook bot url.
* click on the test tab click on "publish bot" button.

# integrate your bot with web channel

* go to this url "https://cloudarrowai.web.app/#/external/module/integration-1211"

* click on new inegration button this page will come:

<p align="center">
<img src="./images/channels.png">
</p>

* select web channel and then give any name for your web integration.
<p align="center">
<img src="./images/webintegration/addintegration.png">
</p>


* now your integration name will be started showing in integration list.
* click on your integration then:
    * enable bot 
    * select bot type (single bot /multi bot)
    * select bot from dropdown.
    <p align="center">
    <img src="./images/webintegration/selectbot.png">
    </p>
    <p align="center">
    <img src="./images/webintegration/integrationdetail.png">
    </p>
* click on the submit button.
* now copy integrationid of your integration.
    <p align="center">
    <img src="./images/webintegration/getintegrationid.png">
    </p>

# chat with your web bot 

* copy below code into html file like webchat.html ,you may give any name to your html file.

   <a href="./webchat.html" download="webchat.html">webchat</a>


* for integration id get your integrationid as mentioned above in integrate your bot with web channel
* to get applicationid click on user icon or application settings from https://cloudarrowai.web.app.
   <p align="center">
    <img src="./images/webintegration/appid.png">
    </p>
* open your bot script in browser or for here open playbot.html on browser
 <p align="center">
 <img src="./images/webintegration/webchat.png">
 </p>

* send your bot welcome message to the bot .

# events  used in webchat script

* in web chat script there some events can be used
 
  ```
  Add event
  open
  close
  hide
  show
  sendCustomMessage

  ```
  * after initializing arrowChat you can call any events mentioned above
  * to initialize arrowChat paste this code ,here appid is your applicationid and integrationid is integration id of that integration which you want to use 
    
```js
     window.ArrowChat.init({
                applicationId: "<appid>",
                applicationName: "WhatsApp bot",
                paramOnce: { //This will be sent only once. It can be used to pass reference id etc
                    
                    applicationId: "<appid>",
                    //
                },
                paramAll: {
                   
                    applicationId: "<appid>",
                    
                        //This will be sent for all conversations
                },
                integrationId: "<integrationId>"
            });
```
 * after initializing arrowChat you can use all the events  


  * **open** from this event chatbot iframe will open .to call this event use below code
     ```
     ArrowChat.open();

    ```

  * **close** from this event chatbot iframe will close .to call this event use below code
    ```
    ArrowChat.close();

    ```
  * **Hide** from hide event chatbot iframe and bot icon will be hide.to call this event use below code
    ```
      ArrowChat.hide();

    ```
  * **show** from show event chatbot iframe and bot will start showig.to call this event use below code

    ```
    ArrowChat.show();

    ```

  * **sendcustommessage** you can send cutom message from this event 

    ```
    ArrowChat.sendCustomMessage("<Custom Message>")
    ```
    * at the place of CustomMessage you can send any message you want.

 * **Add event** from this event  you can add event on server .you can use this event from below code.
 ```
   window.ArrowChat.addExternalEvents("<eventname>", {
                <eventvalue>
                userCreated: "yes"
            }).then(function() {
                console.log("Event Added");
            });
 ```
* here ,eventname is that event which you want to create and in eventvalue whatever event you want set in your event. 

