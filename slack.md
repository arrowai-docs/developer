## integrate slack channel

 * [clickHere]("https://api.slack.com") and create app

 # how to create app

 * select my apps and then click on create new app button

* now popup will open give your app name and select your worspace name from dropdown

* now you see page

<p align="center">
<img src="./images/slack.png">
</p>

* click on "Add features and functionality" and select incoming webhooks

*  and now "Activate Incoming Webhooks" click on toggle and scroll below click on "add new webhook to workspace"

* now you see this page

<p align="center">
<img src="./images/slackwebhook.png">
</p>

* select your channel and click on allow button 

* now you will see you get your url ,from this url you can send message on your channel.you can test this from postman

## how to create channel in your worksapce

* [fromthisurl]("https://app.slack.com/")go to your workspace.

* in the left side menu of your workspace when you hover on channels menu then you will see add icon

* now click on add icon and select create a channel

* now you see a pop-up is open give your channel name and description

* and click on create button.

## integrate your slack app with node js code

* select event sbscription from basic information 

 <p align="center">
 <img src="./images/slackevent.png">
 </p>

* Enable Events by clicking on toggle button

* in the request url input box you have to give your url from where you want to get your response

* your url must be like if you are working on local

 ```
  <ngrokurl>/<appid>/<integration>/incoming/<integration-id>
 ```

* for server

```
<server-url>/<appid>/<integration>/incoming/<integration-id>
```

## create integration for slack

* [clickhere]("https://cloud.arrowai.web.app") and login

* now click on integration menu and click add integration button

* now you will see list of channels select slack channel and give all the details 

* field of slack channel

```
1.token
2.Signing-Secret
3.integration name
```

# How to get token in slack

* in your slack app go to basic information page and now select Oauth and Permissions from features 

* now you see page

<p align="center">
 <img src="./images/slacktoken.png">
 </p>

 * copy oauth token from here

# How to get Signing-Secret

* in your slack app go to basic information page and scroll down and you will see signing secret field 

* from this field you can get your signing secret key

 <p align="center">
 <img src="./images/signinslack.png">
 </p>


# get integration id

* give our integration name and then submit 

* after submit all the details 

* you will see your integration in integration list and from here tou will get your integration id.

















