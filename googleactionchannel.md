# create project in google console

* go to this url:
    https://console.actions.google.com/

* and create new project 
  * select custom in "What kind of Action do you want to build?" and click next button.
  * select blank project in "How do you want to build it?" and click on "start building" button.

* In the Develop, click on settings under invocation  and enter the Invocation and assign Display Name and click on save button.

# integrate your project with arrowai platform

* go to this url https://cloudarrowai.web.app
* create new integration from integration menu 
* set webhook url :<serverurlofbot/ngrokurlofbot>/<application-id>/custom/incoming/1
* write your integration name:"google-action-integration"(you may give any name).
* go to this url https://cloudarrowai.web.app/#/integrations/channels and click on "google-action-integration" enable bot and select bot type.
* after selecteing bot category ,select bot in module name then submit all the changes.
    
# get yaml file

* go to  https://developers.google.com/assistant/actionssdk/gactions  and open that repository,there will be gactions.exe file.

* set path of  gactions.exe in your system PATH variable

* after extracting zip file run command in that repository "./gactions init" and then "./gactions login".

* make repository "googleActionProj"(you may give any name) in  that repository where gactions.exe exits.

* download this file
  <a href="../zip repositories/projnew.zip" download="projnew.zip">projnew</a>

* extract above yaml zip repository in googleActionProj.

* set project-id in googleActionProj/settings/settings.yaml.you can get project-id from 
go to https://console.actions.google.com/ select your project then click on project settings and copy project-id.

* set fulfillment url in googleActionProj/webhooks/ActionsOnGoogleFulfillment.yaml.

* url must be like

    "<server-url/ngrok-url>/<application-id>/Custom/incoming/<integration-id>".

* you can get integrtion-id from [here](https://cloudarrowai.web.app/#/integrations/channels)

* click on integration you have created for here  copy integrationId of  "google-action-integration" 

* run command in googleActionProj (your directory) 

```
  ../gactions deploy preview.
```  

  # test your bot:

   * go to the test tab of your project which you have created on google console.
   * then say talk to <yourinnvocation> .


 