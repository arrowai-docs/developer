# Setting up Gitlab Repo

## Introduction

Our deployment is fully automated using Gitlab CI/CD. You will be provided with a Gitlab Repository where you can publish code, and it will automatically be deployed on Server.

!> If you do not have an authorized Gitlab Repository, then please contact your Team head.

## Setting up Gitlab Repository

The Structure of an Auto Deployment Repository is as below:
```shell
.
├── .gitlab-ci.yml
├── packages
│   ├── package1
│   │   ├── package.json
│   │   ├── arrowai.json
│   │   ├── index.js
│   │   └── ....
│   ├── package2
│   │   ├── package.json
│   │   ├── arrowai.json
│   │   ├── index.js
│   │   └── ....
```

!> .gitlab-ci.yml and arrowai.json are needed for the deployment. Also the directory structure should have packages folder in the base and your modules in individual package folder inside it.

## Gitlab CI YML

If your repository has projects in it, it will have a .gitlab-ci.yml file. In case you have a fresh install, then do the following step:

* Add a file named .gitlab-ci.yml
* Enter the following contents in the file:

```atom
include:
  - remote: 'https://gitlab.com/utkarshxyz/deploymentscripts/-/raw/master/.package-divided.yml'
```

## Packages

ArrowAI is highly modular and Microservice Based Application. Any Application Development is in the format of a **Node Module**. The Build script uses ArrowAI Json and processes the packages. Each package is a *Node Module* with a unique folder name and package name. 

Each Package folder should contain a Package.json and an ArrowAi.json file.

!> Do not forget to add a README.md file which gives a clear documentation of what the package is intended to do as well as other information.


## ArrowAI Json

For Each package should contain an arrowai.json file. This is a Standard File, which is used by the application during the build process as well as during other integrations in the application.

The Structure of an Arrowai.Json File is a below:

```js
{
    "unique_name": "uniqueprojname-75690",
    "type": "apimodule",
    "category": "application",
    "function_deploy": {
        "enabled": "enabled",
        "function_version": "0.0.46"
    },
    "registry_deploy": {
        "registry_version": "0.0.54",
        "forced_deployment": "disabled"
    }
}
```

### Overview of ArrowaI.json Structure

ArrowAI.Json has four different parts:

1. Type information
2. Registry Deployment Information
3. Function Deployment Information
4. Other Configurations

### Type Information

An arrowAi package can be multiple things, an API, a Frontend Component, An Angular Element, a Channel Module or multiple other things. 

In the Type Information, we define the details about the package. The Major components are:

1. **Unique Name**: Every Arrowai Json package has to have a global unique name. Since it will be a part of Multi-repo module, hence it is **Necessary** to have a unique name.

> We suggest you use a unique **single name* and add some random 4 digit numbers to make sure that the package name is unique. 

2. **Type**: The Type field defines the type of Module present in the package Folder. Your package can be one of the following values as type:
    * *apimodule* : An API module is an application api. [See here for details](apimoduleoverview.md)

## Package Types

There are multiple different Package Types than can be created. Some of them are outlined below:

```
1.apimodule
2.uielement_app
3.chatagentmodule

```
* **apimodule** your package type will be apimodule if in your repository you are creating api for arrowai element ui.

* **uielement_app** your package type will be uielement_app if in your repository you are creating element for arrowai platform

* **chatagentmodule** your package type will be chatagentmodule if in your repository you are creating  iframe for chat element



