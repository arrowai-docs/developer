# Create your chatbot
 
 * [clickhere](https://cloudarrowai.web.app/) and login

 * click on the bots from left menu now you can see the list of bots is showing 

 * click on new bot popup will open 

    <p align="center">
     <img src="./images/createbot.png">
     </p>

* select your bot type as per your requirement.

* list of bot type

    * ArrowAi Bot 
    * Dialogflow Bot
    * Knowledgebase Bot
    * Custom Bot
    
* Enter description of your bot

* click on save button.

* now you can see name of bot you have created

* click on the name of bot you have created now you can see,

    <p align="center">
     <img src="./images/bot detail.png">
     </p>

* To trained your bot 

* click on NLP tab and then click on new NLP button .

    <p align="center">
    <img src="./images/create nlp.png">
    </p>

* and from this page you can train your bot.

* now click on flow tab and create new flow and then give name of flow and select which nlp you want to use in this flow.

* and then use drag and drop. drag that tab in which format you want to get your respnse

    <p align="center">
    <img src="./images/botset.png">
    </p>

* you can create many flow as you want.

* now click on test tab and click on publish bot.
   
# Get your Facebook page ready

* Your chatbot will only be available for integration on a Facebook page – not on your personal profile. This means you must either create a Facebook page or have an existing page that you can use with your bot.

* create one at https://www.facebook.com/bookmarks/pages by doing the following:

* Click Create a Page.

* enter page name and category click continue.

# create facebook massenger app

* To publish your chatbot on your Facebook page, you need to create an app that connects ArrowAi and your Facebook page.

* You do this at https://developers.facebook.com/ (make sure you are logged into Facebook).

* First, choose My Apps at the top right of the screen and then choose + Create App.

* Second, give the app a name (e.g., My Bot App) and enter your email, then click Create App ID.

# Set up your Facebook Messenger app

* On the left of your Facebook dashboard, choose the plus symbol after PRODUCTS.

* Find the Messenger tile, and choose Set Up.

* You now see Messenger under PRODUCTS on the left of the screen.

# create new integration for facebook

* now,click on integration menu bar and now you see list of channels will show here

* click on new integration button and select facebook channel after click on facebook channel popup will open

* now paste Generated Token value in access token field. you will get this field from [here]("https://arrowai-docs.gitlab.io/developer/#/facebookchannel?id=get-your-facebook-page-token")

* enter anydata in User Access Token  and <b>remember this will be your verify token value in facebook developer page</b>
 
* give the name of integration you may give any name  and submit 

* after submit now you can see your newly created integration name and you will get integration id value 


# Get your Facebook page token

* On the left of your dashboard, choose Settings just below the Messenger product.

* In Access Tokens section, click Add or Remove Pages.

* A login dialog asks you to confirm your account. Click Continue as ….

* Select your Facebook page, and click Next.

* Click Done.

* Back in the Settings screen, click Generate Token

* Click I Understand, and copy the token, and click Done.

* Go back to the Facebook developet dashboard and choose Products > Messenger > Settings.

* Go to the Webhooks section, and click Add Callback URL.

* In the popup that now appears, enter the values for Callback URL and Verify Token

* callback URL will be like
  <server-url/ng-rok-url>/<applicationid>/facebook/incoming/<integration-id>

* this callback url will be call from get and post both method

* to verify token callbackurl of get method will work and when you send message from facebook callback url of post method will work.hence ,you have to create api for both method with same url.

* you can get inegration id from above.    

* click <b>verify</b> and <b>save</b>

* Once your page has reloaded, click Add Subscriptions for your page.

* Select the first 4 fields:

       * messages

       * messaging_postbacks

       * messaging_optins

       * message_deliveries

# Test your Messenger chatbot

* Go to your Facebook home page (not the bot page) and start a conversation with your bot.

* Click the New Message button at the bottom of the page, and enter the name of your Facebook page.

* Type in text that will trigger intents of your bot.








