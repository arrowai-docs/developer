# API Module Overview
* create repository for api-module.
* run command "npm init" in your repository.
* run command "npm install" in your repository.
* create index.js in repository.
* paste this code into index.js

```js
var express = require('express');
var router = express.Router();
const cors = require('cors');
const app = express();
app.use(cors())
router.get('/',function(req,res){
   console.log(platformObj.DB());
});
module.exports = function (platform) {
    platformObj = platform;
    return router;
}
```
* there is platformObj.DB() function from this function you can get db details.

<table>
<tr>
 <td>mongo</td>
 <td>mongoDb</td>
</tr>
<tr>
 <td>sql</td>
 <td>sqlDb</td>
</tr>
<tr>
 <td>bigQuery</td>
 <td>bigQueryDb</td>
</tr>
<tr>
 <td>redis</td>
 <td>redisCacheDb</td>
</tr>
<tr>
 <td>cosmo</td>
 <td>cosmoDb</td>
</tr>
</table>

* if you are using sqlDb then your api will look like this 
```js
router.post('/apiname',function(req,res){
    

    let pool= platformObj.DB().sqlDb;
 
    pool.getConnection(function(err, connection){
     
    if(err){
      console.log(err)
    }
    connection.changeUser({database : databasename});

  try{  
    var sql="query";
    connection.query(sql, function(err, data) {
      connection.release();
      res.send(data);
      });
}
catch (error) {
    res.send(error);
}
  })
});
```
* if you are using bigQuery then api should look like

```js
router.post('/apiname', function (req, res, next) {
    let bigQueryHelper = platformObj.DB().bigQueryDb;
    try {
        let query = `query`;
        bigQueryHelper.excuteQuery(query).then(data => {

            res.send(data);
        })
    }
    catch (error) {
        console.log(error);
    }


});

```
* from platformObj.DB.yourDB()  you can get all the function name can be used in your api.
* To run your api on local create .env file
```
ARROWAPI_NODE_ENV=production
ARROWAPI_MONGO_HOST_URL=mongodb://mongodb.arrowai.com:27017/platform_production
ARROWAPI_MONGO_USER=platform
ARROWAPI_MONGO_PASSWORD=Utkarsh1234
ARROWAPI_COSMO_HOST_URL=mongodb://arrowcosmodb.documents.azure.com:10255/production_data?ssl=true&replicaSet=globaldb
ARROWAPI_COSMO_USER=arrowcosmodb
ARROWAPI_COSMO_PASSWORD=LqZCXrRE4ZNzwlp4P0q5e6Z8c0Wj6JyMQy9JqutoCXitMoDxAwQHICM7R9tBaDF6tt8MGJ5BnOBxuk0bh8bqbw==
ARROWAPI_REDIS_HOST_URL=arrowai.redis.cache.windows.net
ARROWAPI_REDIS_HOST_PORT=6379
ARROWAPI_REDIS_HOST_PASSWORD=pOcsJb9u+2j88MG2eDQbSMheHC4mthYvPseOkOWaCbc=
ARROWAPI_MYSQL_HOST_URL=mysql.arrowai.com
ARROWAPI_MYSQL_PORT=<port>
ARROWAPI_MYSQL_DATABASE=<databasename>
ARROWAPI_MYSQL_USER=<database_user_name>
ARROWAPI_MYSQL_PASSWORD=<database_password>
ARROWAPI_BIGQUERY_KEY_PATH=
```
# run your code on local:
* to run your code on local :
  run command "adk startapimodulesdk".

* your api url is like :
 ```
 "http://localhost:3000/api-module/<your-apiname>"
 ```
 # run your code on server:
  * first push your code when it will deploy on server you will get url like:
  ```
   <your server url>/api/<your-apiname>
  ```