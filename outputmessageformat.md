# Output Message Format

The System can send the messages in the following format

## Text Response

```js
{
   "messages":[
      {
         "applicationId": <application_id>,
         "botId": "customBot",
         "message":{
            "text": <text_message>,
            "finalStep": true,
            "confidence": 99.66203581986875
         }
      }
   ],
   "wrapper": {
      "integration": "web",
      "message_id": 1592649101897,
      "payload": {
         "integration": "web"
      },
      "applicationId": "web",
      "recipient": {
         "id": "E0274"
      },
      "sender": {
         "id": "bot"
      },
      "sentFromServer": true,
      "sentFromUser": false,
      "sentFromRep": false,
      "timestamp": 1592649101897,
      "uniqueUserId": "E0274",
      "botId": "customApp"
   }
}
```


| Title | Type | Description |
|---|---|---|
| < application_id > | Character  | Here you have to give your application id. |
| < text_message > | String | The text which will be displayed on the chat. |


## Bottom Button Responses

```js
{
    "messages": [
        {
            "applicationId": <application_id>,
            "botId": <bot_id>,    //optional
            "message": {
                "attachment": {
                    "type": "template",
                    "payload": {
                        "template_type": "button_bottom",
                        "text": <text_message>,
                        "buttons": [
                            {
                                "type": "postback",
                                "title": <Button1>,
                                "payload": {
                                    "variable": <variable_name>,
                                    "value": <Button1>,
                                    "text": <Button1>
                                },
                                "variableType": {
                                    "type": "regexParser",
                                    "valueName": <variable_name>_<Button1>,
                                    "values": [
                                        <Button1>
                                    ]
                                }
                            },
                            {
                                "type": "postback",
                                "title": <Button2>,
                                "payload": {
                                    "variable": <variable_name>,
                                    "value": <Button2>,
                                    "text": <Button2>
                                },
                                "variableType": {
                                    "type": "regexParser",
                                    "valueName": <variable_name>_<Button2>,
                                    "values": [
                                        <Button2>
                                    ]
                                }
                            }
                        ]
                    }
                },
                "suggestion": [],
                "finalStep": false,
                "confidence": 100
            }
        }
    ],
    "wrapper": {
        "integration": "web",
        "message_id": "02109560-abab-11ea-9de9-1559e45cbe48",
        "payload": {
            "integration": "web"
        },
        "applicationId": <application_id>,
        "recipient": {
            "id": "E0247"
        },
        "sender": {
            "id": "bot"
        },
        "sentFromServer": true,
        "sentFromUser": false,
        "sentFromRep": false,
        "timestamp": 1592649889229,
        "uniqueUserId": "E0247",
        "botId": <bot_id>        //optional
    }
}
```

| Title | Type | Description |
|---|---|---|
| < application_id > | Character  | Here you have to give your application id. |
| < bot_id > | Character | Provide your Bot Id |
| < text_message > | String  | The text which will be displayed on the chat.|
| < Button1 >, < Button2 > | String | The text which will be displayed on the button. This text should indicate the purpose of the button. |
| < variable_name > | String | The name of button variable and it should be same for all the buttons. |


## Card Response

```js
{
    "messages": [
        {
            "applicationId": <application_id>,
            "botId": <bot_id>,
            "message": {
                "attachment": {
                    "type": "template",
                    "payload": {
                        "template_type": "search_list",
                        "text": <card_name_1>,
                        "list": [
                            {
                                "type": "card_list",
                                "text": <card_name_1>,
                                "dynamic": false,
                                "id": "",
                                "loopVariable": "",
                                "maxCount": "",
                                "title": <card_name_1>,
                                "image": <image_url>,
                                "url": <image_url>,
                                "default_action": {
                                    "type": "web_url",
                                    "url": <url>,
                                    "webview_height_ratio": "tall"
                                },
                                "buttons": [
                                    {
                                        "type": "postback",
                                        "title": <Button1>,
                                        "variableName": <Button1>,
                                        "payload": {
                                            "variable": <variable_name>,
                                            "value": <Button1>,
                                            "text": <Button1>
                                        },
                                        "variableType": {
                                            "type": "regexParser",
                                            "valueName": <variable_name>_<Button1>,
                                            "values": [
                                                <Button1>
                                            ]
                                        }
                                    }
                                ]
                            },
                            {
                                "type": "card_list",
                                "text": <card_name_1>,
                                "dynamic": false,
                                "id": "",
                                "loopVariable": "",
                                "maxCount": "",
                                "title": <card_name_1>,
                                "image": <image_url>,
                                "url": <image_url>,
                                "default_action": {
                                    "type": "web_url",
                                    "url": <url>,
                                    "webview_height_ratio": "tall"
                                },
                                "buttons": [
                                    {
                                        "type": "postback",
                                        "title": <Button2>,
                                        "variableName": <Button2>,
                                        "payload": {
                                            "variable": <variable_name>,
                                            "value": <Button2>,
                                            "text": <Button2>
                                        },
                                        "variableType": {
                                            "type": "regexParser",
                                            "valueName": <variable_name>_<Button2>,
                                            "values": [
                                                <Button2>
                                            ]
                                        }
                                    }
                                ]
                            }
                        ]
                    }
                },
                "suggestion": [
                ],
                "finalStep": false,
                "confidence": 99.90233883146944,
                "recommendations": [
                    
                ]
            }
        }
    ],
    "wrapper": {
        "integration": "web",
        "message_id": "02109560-abab-11ea-9de9-1559e45cbe48",
        "payload": {
            "integration": "web"
        },
        "applicationId": <application_id>,
        "recipient": {
            "id": "E0247"
        },
        "sender": {
            "id": "bot"
        },
        "sentFromServer": true,
        "sentFromUser": false,
        "sentFromRep": false,
        "timestamp": 1592651404348,
        "uniqueUserId": "E0247",
        "botId": <bot_id>
    }
}
```

| Title | Type | Description |
|---|---|---|
| < application_id > | Character  | Here you have to give your application id. |
| < text_message > | String | This text will be displayed in chat |
| < card_name_1 >, < card_name_2 > | String  | The text displayed on the card this text should indicate the purpose of the card |
| < image_url > | URL | Link of image which you want to be displayed. It should be JPEG, JPG or PNG file |
| < card_description > | String | The text displayed on the card which involves the description of that particular card |
| < Button1 >, < Button2 > | String | The text displayed on the button. This text should indicate the purpose of the button. |
| < variable_name > | String | The name of variables |


## Audio Response

```js

```

## Video Response

```js
{
    "messages": [
        {
            "applicationId": <application_id>,
            "botId": <bot_id>,
            "message": {
                "attachment": {
                    "type": "template",
                    "payload": {
                        "template_type": "video",
                        "text": <text_message>,
                        "video": {
                            "properties": {
                                "variableValue": "",
                                "value": "",
                                "checkVariable": false,
                                "variableName": "",
                                "condition": "",
                                "type": "",
                                "suggestion": "",
                                "buttonType": "",
                                "repeatOnUnReplied": false,
                                "buttons": [],
                                "card_list": [],
                                "video": [
                                    {
                                        "dynamic": false,
                                        "videoObj": {
                                            "loopVariable": "",
                                            "maxCount": "",
                                            "type": "url",
                                            "url": <url>,
                                            "title": <video_title>,
                                            "short_code": "",
                                            "order": ""
                                        }
                                    }
                                ]
                            },
                            "component": "chat.video",
                            "componentModule": {
                                "variableList": [],
                                "variable": []
                            },
                            "name": "Video",
                            "type": "story_intent",
                            "patterns": []
                        }
                    }
                },
                "suggestion": [],
                "finalStep": false,
                "confidence": 99.78330628326282,
                "recommendations": [
                    
                ]
            }
        }
    ],
    "wrapper": {
        "integration": "web",
        "message_id": "02109560-abab-11ea-9de9-1559e45cbe48",
        "payload": {
            "integration": "web"
        },
        "applicationId": <application_id>,
        "recipient": {
            "id": "E0247"
        },
        "sender": {
            "id": "bot"
        },
        "sentFromServer": true,
        "sentFromUser": false,
        "sentFromRep": false,
        "timestamp": 1592653512320,
        "uniqueUserId": "E0247",
        "botId": <bot_id>
    }
}
```

| Title | Type | Description |
|---|---|---|
| < application_id > | Character  | Here you have to give your application id. |
| < text_message > | String | This text will be displayed in chat |
| < url > | URL | Link of video which you want to be displayed. It should be mp4 file |
| < video_title > | String  | The title which will be displayed above the video |


## Image Reponse 

```js
{
    "messages": [
        {
            "applicationId": <application_id>,
            "botId": <bot_id>,
            "message": {
                "attachment": {
                    "type": "image",
                    "payload": {
                        "text": <text_message>,
                        "images": {
                            "properties": {
                                "variableValue": "",
                                "value": "",
                                "checkVariable": false,
                                "variableName": "",
                                "condition": "",
                                "type": "",
                                "suggestion": "",
                                "buttonType": "",
                                "repeatOnUnReplied": false,
                                "buttons": [],
                                "card_list": [],
                                "conditions": [],
                                "image": [
                                    {
                                        "dynamic": false,
                                        "imageobj": {
                                            "loopVariable": "",
                                            "maxCount": "",
                                            "type": "url",
                                            "url": <image_url>,
                                            "title": <image_title>,
                                            "short_code": "",
                                            "order": ""
                                        }
                                    }
                                ]
                            },
                            "component": "chat.image",
                            "componentModule": {
                                "variableList": [],
                                "variable": []
                            },
                            "name": "Image",
                            "type": "story_intent",
                            "patterns": []
                        }
                    }
                },
                "suggestion": [],
                "finalStep": false,
                "confidence": 99.78330628326282,
                "recommendations": [
                    
                ]
            }
        }
    ],
    "wrapper": {
        "integration": "web",
        "message_id": "02109560-abab-11ea-9de9-1559e45cbe48",
        "payload": {
            "integration": "web"
        },
        "applicationId": <application_id>,
        "recipient": {
            "id": "E0247"
        },
        "sender": {
            "id": "bot"
        },
        "sentFromServer": true,
        "sentFromUser": false,
        "sentFromRep": false,
        "timestamp": 1592654628455,
        "uniqueUserId": "E0247",
        "botId": <bot_id>
    }
}
```

| Title | Type | Description |
|---|---|---|
| < application_id > | Character  | Here you have to give your application id. |
| < text_message > | String | This text will be displayed in chat |
| < image_url > | URL | Link of image which you want to be displayed. It should be JPEG, JPG, PNG file |
| < image_title > | String  | The title which will be displayed above the image |
