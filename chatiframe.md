## How to create iframe in chat window:
* There is "adk" file.
<a href="./chatadk.zip" download="chatadk.zip">adk</a>

* run command in your installed zip file "npm i -g adk"

## How to test your iframe  on local:
* To run this use command "adk startuisdk"
* and then go to the browser open url:
	"https://cloudarrowai.web.app/#/chat?dev=true"
* you will get whatever you have written in your template.html.
* to change the title go to the manifest.json and write your title name.
* you can do your code in app.js

