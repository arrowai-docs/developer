## Web integration

* add this script to your web page
```js
  <script>
        function arrowaiInit() {
            window.ArrowChat.init({
                applicationId:<your applicationId>,
                applicationName: "Name for your bot ",
                paramOnce: { //This will be sent only once. It can be used to pass reference id etc
                    key: <value>
                },
                paramAll: {
                    key: <value>
                        //This will be sent for all conversations
                },
                integrationId: "<integrationId>"
            });
        }
    </script>
    <script onload="arrowaiInit()" src="https://arrowaichat.web.app/chat/script.js"></script>
    </script>
    ```
