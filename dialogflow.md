# Building your First Dialog Flow Bot

## Create Dialog Flow Bot

* Go to URL [here](https://cloudarrowai.web.app).
* First 'Sign Up', if you are already registered then 'Sign In'.
* Click 'Bots' menu and then on 'New Bot' to create your new Dialog Flow bot.

![logo](images/bot.PNG)

* Then pop-up will open in that select type as Dialog-Flow,Name what you want and for credentials JSON you to do following thing-

![logo](images/dialog_flow_popup.PNG)

# Create New Agent in Dialog Flow

* Go to URL-
  [here](https://dialogflow.cloud.google.com)
  , Enable dialog flow plugin.

* Create a new agent as per your choice.

![logo](images/dialog_flow_agent.PNG) 

* Give the name what you want and click the ‘Create’ button.

![logo](images/create_agent.PNG)

* Then, in the left side menu there is Intents,Entities,Fulfilment etc.There is default intents and entities also. You can also add new Intents and Entities.

![logo](images/agent_menu.PNG)

* After this get back to your service page.It will look as below, currently you don’t have any project.


# Create Service Account

* Create service account,
    Go to URL- [here](https://cloud.google.com/iam/docs/creating-managing-service-accounts#creating)
    In this,click on 'Go to the Service accounts page'.

![logo](images/service_link.PNG) 

* Click select a project, in pop-up click All. List will show.Select the agent name that you enter     and Click open.

![logo](images/service_account.PNG)


* Click on ‘+Create Service Account’

![logo](images/service_create_account.PNG)

* Fill the detail in the textbox,service name and description is totally as per your choice.
Service account Id automatically generated.Remaining part is optional.

![logo](images/service_detail.PNG)

* Click on the email field in row,Then in the Keys tab, click on add key->create new key->create. Make sure key type will be of JSON.

* After you get JSON file.Copy content in this file and paste in 'Enter credentials Json' field occurs at the time of creation of dialog flow bot. Save it.

# Bot Connector

* To test a dialog flow bot go through Custom Bot Connector [here](botconnector.md).



