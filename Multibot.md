# create multi bot

 * [clickhere](https://cloudarrowai.web.app/) and select bot from left sidbar menu 

 * click on multi bot tab 

 * click on new multi bot button 

 * now you see there is two tabs added bots and setting

 * in added bots give your bot name and select bot name from dropdown and click on add more button and select another bot name and click add more you can add more bot like this and click on save button.

## multi bot settings

* click on settings tab in multi bot of bot module ,now you see there is 3 hooks
 
 ```
 pre NLP Hooks
 post NLP Hooks
 post Response Hooks
 ```
 # pre NLP Hooks

  * in this field of multi bot settings we set url on which url it will go before geting the nlp of bot.

 # post NLP Hooks

   * in this field you will give API url ,this url will be called when  it will get the nlp of bot .

    * now set your url in this field and click on save button .

    * from this hook in our multi bot if  user said anything like "How are you "  and that sentence is not set as traning phrase in any bot in that case you can use post nlp hooks .

    * from post NLP Hooks api you will get the matchscore(confidence)  of each and every bot you have set.

    * "confidence" is whatever user said through bot how much the sentence matched by any training phrase of bots

    * if the sentence said by user did not match with any training phrase then you can give response from that bot you want.

    ```js
    router.post('/postnlp',function(req,res){
    console.log(req.body);
    var maxConfidence=Math.max.apply(Math, req.body.map(function(o) { return o.nlpResponse.matchScore; }))
    if(maxConfidence>=0.8){
        // <yourcode>
    }
    else{
    var json={
    bot: {
        bot_id: '<botid>',
        bot_text: '<botname>',
        language: 'en',
        type: '',
        customUrl: '<customurl>',
        clientSecret: '<customuerl>',
        id: '<botid>'
    },
    nlpResponse: {
        botPlatform: '<bottype>',
        matchScore: 0.5,
        matchedIntent: '<intentname>',
        botId: '<botid>',
        matchIntentId:"<matchIntentId>"
    },
    
    }
    
    res.send(json)
    }
    })
    ```

# post Response Hooks

* in this hook you will set url which you want to be called whenever multibot's bot will give response .
* when multi bot's bot will give response after that this post Response Hooks api will be called
* from this post Response Hooks api you can do the changes in bot response.
```js
router.post('/postResponseHooks',function(req,res){
  
  if(req.body.messages[0].message.hasOwnProperty('attachment')){
    res.send(req.body);

  }
  else{
    if(req.body.messages[0].message.text.includes('<*smiley*>')){
      var text=req.body.messages[0].message.text;
      var msg=text.replace("<*smiley*>"," ");
      console.log(msg);
          var json={
                      "messages":[
                                          {
                                              "applicationId":req.body.messages[0].applicationId ,
                                              "botId": "customBot",
                                              "message":{
                                                "text": msg+" 😃",
                                                "finalStep": true,
                                                "confidence":  req.body.messages[0].message.confidence
                                              }
                                          }
                                    ],
                          "wrapper": {
                            "integration":req.body.wrapper.integration,
                            "message_id": req.body.wrapper.message_id,
                            
                            "applicationId": req.body.wrapper.applicationId,
                            "recipient": {
                                "id": req.body.wrapper.recipient.id
                            },
                            
                            "sentFromServer": true,
                            "sentFromUser": false,
                            "sentFromRep": false,
                            "timestamp": req.body.wrapper.timestamp,
                            "uniqueUserId": req.body.wrapper.uniqueUserId,
                            "botId": req.body.wrapper.botId
                          }
                }
     res.send(json);
      }
      else if(req.body.messages[0].message.text.includes('{{sugg}}')){
        var found = [],         
        rxp = /{{([^}]+)}}/g,
        str = req.body.messages[0].message.text,
        curMatch;
      while( curMatch = rxp.exec( str ) ) {
        found.push( curMatch[1] );
    }
    let buttons=[];
   let msg= str.replace(/\s?\{{[^}]+\}}/g, ' '); 
   for(i=0;i<found.length;i++){
   var button= {
      "title": found[i],
      "payload": {"variable": found[i],"value": found[i],"text": found[i]},
      "variableType": {"type": "regexParser","valueName": found[i],"values": [found[i]]}
  }
  buttons.push(button);
   }
  var json= {
    "messages": [
        {
            "applicationId": req.body.messages[0].applicationId,
            "botId": req.body.messages[0].botId,    //optional
            "message": {
                "attachment": {
                    "type": "template",
                    "payload": {
                        "template_type": "button_bottom",
                        "text": msg,
                        "buttons": buttons
                    }
                },
                "suggestion":[],
                "finalStep": false,
                "confidence": req.body.messages[0].message.confidence
            }
        }
    ],
    "wrapper": {
        "integration":"web",
        "message_id": req.body.wrapper.message_id,
        "payload": {
            "integration": req.body.wrapper.integration
        },
        "applicationId": req.body.messages[0].applicationId,
        "recipient": {
            "id": req.body.wrapper.recipient.id
        },
        "sender": {
            "id": "bot"
        },
        "sentFromServer": true,
        "sentFromUser": false,
        "sentFromRep": false,
        "timestamp": req.body.wrapper.timestamp,
        "uniqueUserId": req.body.wrapper.uniqueUserId,
        "botId": req.body.messages[0].botId        //optional
    }
}
  res.send(json);
      }
      else{
              res.send(req.body);
      }
  }
  
  
  })

```
* in kb bot if user set <smiley> in response then from this api we will check <smiley> is present in response if it is present then it will give response that you have set.







   
